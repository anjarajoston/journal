/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import connection.ConnectionDB;
import dao.ObjetBdd;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author User_HP
 */
public class Contenu extends ObjetBdd{
    int id;
    int idType;
    int idLieu;
    String titre;
    String visuel;
    String body;
    Date date1;
    Date date2;
    String lieu;
    int idUtilisateur;
    int etat;
    Timestamp datePublication;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdType() {
        return idType;
    }

    public void setIdType(int idType) {
        this.idType = idType;
    }

    public int getIdLieu() {
        return idLieu;
    }

    public void setIdLieu(int idLieu) {
        this.idLieu = idLieu;
    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public String getVisuel() {
        return visuel;
    }

    public void setVisuel(String visuel) {
        this.visuel = visuel;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public Date getDate1() {
        return date1;
    }

    public void setDate1(Date date1) {
        this.date1 = date1;
    }

    public Date getDate2() {
        return date2;
    }

    public void setDate2(Date date2) {
        if( date2 == null ){
            setDate2(new Date(System.currentTimeMillis()));
        }
        this.date2 = date2;
    }

    public String getLieu() {
        return lieu;
    }

    public void setLieu(String lieu) {
        this.lieu = lieu;
    } 

    public int getIdUtilisateur() {
        return idUtilisateur;
    }

    public void setIdUtilisateur(int idUtilisateur) {
        this.idUtilisateur = idUtilisateur;
    }

    public int getEtat() {
        return etat;
    }

    public void setEtat(int etat) {
        this.etat = etat;
    }

    public Timestamp getDatePublication() {
        return datePublication;
    }

    public void setDatePublication(Timestamp datePublication) {
        this.datePublication = datePublication;
    }

    @Override
    public String toString() {
        return "Contenu{" + "id=" + id + ", idType=" + idType + ", idLieu=" + idLieu + ", titre=" + titre + ", visuel=" + visuel + ", body=" + body + ", date1=" + date1 + ", date2=" + date2 + ", lieu=" + lieu + ", idUtilisateur=" + idUtilisateur + ", etat=" + etat + ", datePublication=" + datePublication + '}';
    }
    
    public List<Contenu> rechercheEtat(Recherche recherche) throws Exception {
        List<Contenu> contenus = new ArrayList<>();
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try {
            ConnectionDB con = new ConnectionDB();
            connection = con.getConnection("Postgres");
            String sql = "select  * from contenu where etat = "+recherche.getEtat();
            System.out.println(" req =>>>>>>>>>>>>>> "+sql);
            preparedStatement = connection.prepareStatement(sql);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()){
                Contenu contenu = new Contenu();
                contenu.setId(resultSet.getInt("id"));
                contenu.setIdType(resultSet.getInt("idType"));
                contenu.setIdLieu(resultSet.getInt("idlieu"));
                contenu.setTitre(resultSet.getString("titre"));
                contenu.setVisuel(resultSet.getString("visuel"));
                contenu.setBody(resultSet.getString("body"));
                contenu.setDate1(resultSet.getDate("date1"));
                contenu.setDate2(resultSet.getDate("date2"));
                contenu.setLieu(resultSet.getString("lieu"));
                contenu.setEtat(resultSet.getInt("etat"));
                contenus.add(contenu);
            }
        } catch (Exception exception){
            exception.printStackTrace();
            throw exception;
        } finally {
            if(resultSet != null) resultSet.close();
            if(preparedStatement != null) preparedStatement.close();
            if(connection != null) connection.close();
        }
        return contenus;
    }

    public List<Contenu> recherche(Recherche recherche) throws Exception {
        List<Contenu> contenus = new ArrayList<>();
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try {
            ConnectionDB con = new ConnectionDB();
            connection = con.getConnection("Postgres");
            String sql = "select  * from contenu where " +
                    "(titre like '%"+recherche.getMot()+"' or " +
                    "titre like '"+recherche.getMot()+"%' or " +
                    "titre like '%"+recherche.getMot()+"%' or " +
                    "body like '%"+recherche.getMot()+"' or " +
                    "body like '"+recherche.getMot()+"%' or " +
                    "body like '%"+recherche.getMot()+"%') and etat = 2";
            System.out.println(" req =>>>>>>>>>>>>>> "+sql);
            preparedStatement = connection.prepareStatement(sql);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()){
                Contenu contenu = new Contenu();
                contenu.setId(resultSet.getInt("id"));
                contenu.setIdType(resultSet.getInt("idType"));
                contenu.setIdLieu(resultSet.getInt("idlieu"));
                contenu.setTitre(resultSet.getString("titre"));
                contenu.setVisuel(resultSet.getString("visuel"));
                contenu.setBody(resultSet.getString("body"));
                contenu.setDate1(resultSet.getDate("date1"));
                contenu.setDate2(resultSet.getDate("date2"));
                contenu.setLieu(resultSet.getString("lieu"));
                contenu.setEtat(resultSet.getInt("etat"));
                contenu.setDatePublication(resultSet.getTimestamp("datepublication"));
                contenus.add(contenu);
            }
        } catch (Exception exception){
            exception.printStackTrace();    
            throw exception;
        } finally {
            if(resultSet != null) resultSet.close();
            if(preparedStatement != null) preparedStatement.close();
            if(connection != null) connection.close();
        }
        return contenus;
    }

    public List<Contenu> oneLoad() throws Exception {
        List<Contenu> contenus = new ArrayList<>();
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try {
            ConnectionDB con = new ConnectionDB();
            connection = con.getConnection("Postgres");
            String sql = "select * from contenu where datepublication <= now() and etat = 2 offset 0 limit 3";
            preparedStatement = connection.prepareStatement(sql);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()){
                Contenu contenu = new Contenu();
                contenu.setId(resultSet.getInt("id"));
                contenu.setIdType(resultSet.getInt("idType"));
                contenu.setIdLieu(resultSet.getInt("idlieu"));
                contenu.setTitre(resultSet.getString("titre"));
                contenu.setVisuel(resultSet.getString("visuel"));
                contenu.setBody(resultSet.getString("body"));
                contenu.setDate1(resultSet.getDate("date1"));
                contenu.setDate2(resultSet.getDate("date2"));
                contenu.setLieu(resultSet.getString("lieu"));
                contenu.setDatePublication(resultSet.getTimestamp("datepublication"));
                contenus.add(contenu);
            }
        } catch (Exception exception){
            throw exception;
        } finally {
            if(resultSet != null) resultSet.close();
            if(preparedStatement != null) preparedStatement.close();
            if(connection != null) connection.close();
        }
        return contenus;
    }
    
    public void update(Contenu contenu) throws Exception {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try {
            ConnectionDB con = new ConnectionDB();
            connection = con.getConnection("Postgres");
            String sql = "update contenu set etat=2 , datepublication = '"+contenu.getDatePublication()+"' where id ="+contenu.getId();
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.executeUpdate();
        } catch (Exception exception){
            throw exception;
        } finally {
            if(resultSet != null) resultSet.close();
            if(preparedStatement != null) preparedStatement.close();
            if(connection != null) connection.close();
        }
    }

    public List<Contenu> pagination(int page) throws Exception {
        List<Contenu> contenus = new ArrayList<>();
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try {
            page = page -1;
            ConnectionDB con = new ConnectionDB();
            connection = con.getConnection("Postgres");
            String sql = "select * from contenu where etat = 2 offset "+(page*3)+" limit 3";
            preparedStatement = connection.prepareStatement(sql);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()){
                Contenu contenu = new Contenu();
                contenu.setId(resultSet.getInt("id"));
                contenu.setIdType(resultSet.getInt("idType"));
                contenu.setIdLieu(resultSet.getInt("idlieu"));
                contenu.setTitre(resultSet.getString("titre"));
                contenu.setVisuel(resultSet.getString("visuel"));
                contenu.setBody(resultSet.getString("body"));
                contenu.setDate1(resultSet.getDate("date1"));
                contenu.setDate2(resultSet.getDate("date2"));
                contenu.setLieu(resultSet.getString("lieu"));
                contenu.setDatePublication(resultSet.getTimestamp("datepublication"));
                contenus.add(contenu);
            }
        } catch (Exception exception){
            throw exception;
        } finally {
            if(resultSet != null) resultSet.close();
            if(preparedStatement != null) preparedStatement.close();
            if(connection != null) connection.close();
        }
        return contenus;
    }

    //@Override
    public ArrayList<Object> findAll() throws Exception {
        //return super.find(c); //To change body of generated methods, choose Tools | Templates.
        ArrayList<Object> objects = new ArrayList<>();
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try {
            ConnectionDB con = new ConnectionDB();
            connection = con.getConnection("Postgres");
            String sql = "select * from contenu where datepublication <= now() and etat = 2 order by id";
            preparedStatement = connection.prepareStatement(sql);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()){
                Contenu contenu = new Contenu();
                contenu.setId(resultSet.getInt("id"));
                contenu.setIdType(resultSet.getInt("idType"));
                contenu.setIdLieu(resultSet.getInt("idlieu"));
                contenu.setTitre(resultSet.getString("titre"));
                contenu.setVisuel(resultSet.getString("visuel"));
                contenu.setBody(resultSet.getString("body"));
                contenu.setDate1(resultSet.getDate("date1"));
                contenu.setDate2(resultSet.getDate("date2"));
                contenu.setLieu(resultSet.getString("lieu"));
                contenu.setDatePublication(resultSet.getTimestamp("datepublication"));
                objects.add(contenu);
            }
        } catch (Exception e) {
            throw e;
        } finally {
            if(resultSet != null) resultSet.close();
            if(preparedStatement != null) preparedStatement.close();
            if(connection != null) connection.close();
        }
        return objects;
    }
    
    /*public static void main(String[] args) throws Exception {
        Contenu contenu = new Contenu();
        contenu.setEtat(2);
        ArrayList<Object> objects = contenu.find(new ConnectionDB().getConnection("Postgres"));
    }*/
}
