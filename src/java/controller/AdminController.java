/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import dao.HibernateDao;
import java.sql.Timestamp;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import model.Admin;
import model.Contenu;
import model.Lieu;
import model.Recherche;
import model.Type;
import model.Utilisateur;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 *
 * @author User_HP
 */
 @Controller
public class AdminController {
    @Autowired
    HibernateDao dao;
         
    @RequestMapping(value="/admin-login")
    public String loginAdmin(){
        return "loginAdmin";
    }
    
    @RequestMapping(value="/loginAdmin")
    public String login(HttpServletRequest request, Model model) throws Exception {
        model.addAttribute("pseudo",request.getParameter("pseudo"));
        model.addAttribute("password",request.getParameter("password"));
        Admin admin = new Admin();
        admin.setPseudo(request.getParameter("pseudo"));
        admin.setPassword(request.getParameter("password"));
        List<Admin> admins = dao.authentificationAdmin(Admin.class,admin);
        if( !admins.isEmpty() ){
            admin = admins.get(0);
            int size = new Contenu().findAll().size();
            if( size%3 == 0 ){
                model.addAttribute("nbrPage",size/3);
            } else {
                if( (int)size/3 == 0 ){
                    model.addAttribute("nbrPage",0);
                } else {
                    model.addAttribute("nbrPage",(size/3)+1);
                }
            }
            Contenu contenu = new Contenu();
            contenu.setEtat(2);
            model.addAttribute("list",contenu.findAll());
            model.addAttribute("admin", admin);
            model.addAttribute("contenus",new Contenu().oneLoad());
            return "back-office-admin";
        }
        model.addAttribute("message","Error!");
        return "loginAdmin";
    }
    
    @RequestMapping(value="/rechercheAdmin")
    public String recherche(HttpServletRequest request, Model model) throws Exception {
        String page = "";
        if( Integer.parseInt(request.getParameter("etat")) == 1){
           Recherche recherche = new Recherche();
            recherche.setEtat(Integer.parseInt(request.getParameter("etat")));
            Contenu contenu = new Contenu();
            contenu.setEtat(1);
            model.addAttribute("list",contenu.findAll());
            int size = contenu.findAll().size();
            if( size%3 == 0 ){
                model.addAttribute("nbrPage",size/3);
            } else {
                if( (int)size/3 == 0 ){
                    model.addAttribute("nbrPage",0);
                } else {
                    model.addAttribute("nbrPage",(size/3)+1);
                }
            }
            model.addAttribute("contenus",new Contenu().rechercheEtat(recherche));

            model.addAttribute("recherche","yes");
        } else if(Integer.parseInt(request.getParameter("etat")) == 2 ) {
            Recherche recherche = new Recherche();
            recherche.setMot("a");
            Contenu contenu = new Contenu();
            contenu.setEtat(2);
            model.addAttribute("list",contenu.findAll());
            int size = contenu.findAll().size();
            if( size%3 == 0 ){
                model.addAttribute("nbrPage",size/3);
            } else {
                if( (int)size/3 == 0 ){
                    model.addAttribute("nbrPage",0);
                } else {
                    model.addAttribute("nbrPage",(size/3)+1);
                }
            }
            model.addAttribute("contenus",new Contenu().recherche(recherche));

            model.addAttribute("recherche","yes");
        }
        else {
            Recherche recherche = new Recherche();
            recherche.setMot(request.getParameter("mot"));
            Contenu contenu = new Contenu();
            contenu.setEtat(2);
            model.addAttribute("list",contenu.findAll());
            int size = contenu.findAll().size();
            if( size%3 == 0 ){
                model.addAttribute("nbrPage",size/3);
            } else {
                if( (int)size/3 == 0 ){
                    model.addAttribute("nbrPage",0);
                } else {
                    model.addAttribute("nbrPage",(size/3)+1);
                }
            }
            model.addAttribute("contenus",new Contenu().recherche(recherche));

            model.addAttribute("recherche","yes");
        }
        page += "back-office-admin";
        return page;
    }
    
    @RequestMapping(value="/validation")
    public String validation(HttpServletRequest request, Model model) throws Exception {
        Contenu contenu = new Contenu();
        contenu.setId(Integer.parseInt(request.getParameter("idContenu")));
        String date = request.getParameter("datePub").replace("T", " ")+":00.00";
        contenu.setDatePublication(Timestamp.valueOf(date));
        contenu.update(contenu);
        
        Recherche recherche = new Recherche();
        recherche.setMot("a");
        contenu.setEtat(2);
        model.addAttribute("list",contenu.findAll());
        int size = contenu.findAll().size();
        if( size%3 == 0 ){
            model.addAttribute("nbrPage",size/3);
        } else {
            if( (int)size/3 == 0 ){
                model.addAttribute("nbrPage",0);
            } else {
                model.addAttribute("nbrPage",(size/3)+1);
            }
        }
        model.addAttribute("contenus",new Contenu().recherche(recherche));

        return "back-office-admin";
    }
    
}
