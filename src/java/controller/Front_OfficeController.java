/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import dao.HibernateDao;
import javax.servlet.http.HttpServletRequest;
import model.Contenu;
import model.Lieu;
import model.Recherche;
import model.Type;
import model.Utilisateur;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 *
 * @author User_HP
 */
@Controller
public class Front_OfficeController {
    @Autowired
    HibernateDao dao;

    @RequestMapping(value="/front-office")
    public String front(Model model) throws Exception {
        Contenu contenu = new Contenu();
        model.addAttribute("list",contenu.findAll());
        int size = contenu.findAll().size();
        if( size%3 == 0 ){
            model.addAttribute("nbrPage",size/3);
        } else {
            if( (int)size/3 == 0 ){
                model.addAttribute("nbrPage",0);
            } else {
                model.addAttribute("nbrPage",(size/3)+1);
            }
        }
        model.addAttribute("contenus",new Contenu().oneLoad());
        return "front-office";
    }

    @RequestMapping(value="/recherche")
    public String recherche(HttpServletRequest request, Model model) throws Exception {
        Recherche recherche = new Recherche();
        recherche.setMot(request.getParameter("mot"));
        Contenu contenu = new Contenu();
        contenu.setEtat(2);
        model.addAttribute("list",contenu.findAll());
        int size = contenu.findAll().size();
        if( size%3 == 0 ){
            model.addAttribute("nbrPage",size/3);
        } else {
            if( (int)size/3 == 0 ){
                model.addAttribute("nbrPage",0);
            } else {
                model.addAttribute("nbrPage",(size/3)+1);
            }
        }
        model.addAttribute("contenus",new Contenu().recherche(recherche));

        model.addAttribute("recherche","yes");
        return "front-office";
    }

    @RequestMapping(value="/pagination")
    public String pagination(HttpServletRequest request, Model model) throws Exception {
        model.addAttribute("contenus",new Contenu().pagination(Integer.parseInt(request.getParameter("num"))));
        Contenu contenu = new Contenu();
        contenu.setEtat(2);
        model.addAttribute("list",contenu.findAll());
        int size = new Contenu().findAll().size();
        if( size%3 == 0 ){
            model.addAttribute("nbrPage",size/3);
        } else {
            if( (int)size/3 == 0 ){
                model.addAttribute("nbrPage",0);
            } else {
                model.addAttribute("nbrPage",(size/3)+1);
            }
        }
        if(request.getParameter("page").compareToIgnoreCase("front")==0){
            return "front-office";
        } else {
            Utilisateur utilisateur = new Utilisateur();
            utilisateur.setId(Integer.parseInt(request.getParameter("utilisateur")));
            model.addAttribute("utilisateur", utilisateur);
            model.addAttribute("types",dao.findAll(Type.class));
            model.addAttribute("lieux",dao.findAll(Lieu.class));
            return "back-office";
        }
    }
}
