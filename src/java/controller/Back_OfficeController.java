/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import dao.HibernateDao;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import model.Contenu;
import model.Lieu;
import model.Type;
import model.Utilisateur;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 *
 * @author User_HP
 */
@Controller
public class Back_OfficeController {
    @Autowired
    HibernateDao dao;
    
    @RequestMapping(value="/back-office")
    public String back(){
        return "login";
    }
    
    @RequestMapping(value="/login")
    public String login(HttpServletRequest request, Model model) throws Exception {
        model.addAttribute("pseudo",request.getParameter("pseudo"));
        model.addAttribute("password",request.getParameter("password"));
        Utilisateur utilisateur = new Utilisateur();
        utilisateur.setPseudo(request.getParameter("pseudo"));
        utilisateur.setPassword(request.getParameter("password"));
        List<Utilisateur> utilisateurs = dao.authentification(Utilisateur.class,utilisateur);
        if( !utilisateurs.isEmpty() ){
            utilisateur = utilisateurs.get(0);
            model.addAttribute("types",dao.findAll(Type.class));
            model.addAttribute("lieux",dao.findAll(Lieu.class));
            int size = new Contenu().findAll().size();
            if( size%3 == 0 ){
                model.addAttribute("nbrPage",size/3);
            } else {
                if( (int)size/3 == 0 ){
                    model.addAttribute("nbrPage",0);
                } else {
                    model.addAttribute("nbrPage",(size/3)+1);
                }
            }
            Contenu contenu = new Contenu();
            contenu.setEtat(2);
            model.addAttribute("list",contenu.findAll());
            model.addAttribute("utilisateur", utilisateur);
            model.addAttribute("contenus",new Contenu().oneLoad());
            return "back-office";
        }
        model.addAttribute("message","Error!");
        return "login";
    }
    
    @RequestMapping(value="/addContenu")
    public String saveContenu(HttpServletRequest request, Model model) throws Exception {
        Contenu contenu = new Contenu();
        contenu.setIdUtilisateur(Integer.parseInt(request.getParameter("utilisateur")));
        contenu.setTitre(request.getParameter("titre"));
        contenu.setVisuel(request.getParameter("visuel"));
        contenu.setBody(request.getParameter("body"));
        contenu.setIdType(Integer.parseInt(request.getParameter("type")));
        contenu.setDate1(Date.valueOf(request.getParameter("date1")));
        contenu.setLieu(request.getParameter("toerana"));
        if( request.getParameter("date2") == "" ){
        } else {
            contenu.setDate2(Date.valueOf(request.getParameter("date2")));
        }
        String date = request.getParameter("datePub").replace("T", " ")+":00.00";
        contenu.setDatePublication(Timestamp.valueOf(date));
        contenu.setIdLieu(Integer.parseInt(request.getParameter("lieu")));
        contenu.create(null);

        Utilisateur utilisateur = new Utilisateur();
        utilisateur.setId(Integer.parseInt(request.getParameter("utilisateur")));
        model.addAttribute("utilisateur", utilisateur);
        model.addAttribute("types",dao.findAll(Type.class));
        model.addAttribute("lieux",dao.findAll(Lieu.class));
        model.addAttribute("contenus",new Contenu().oneLoad());
        int size = contenu.findAll().size();
        if( size%3 == 0 ){
            model.addAttribute("nbrPage",size/3);
        } else {
            if( (int)size/3 == 0 ){
                model.addAttribute("nbrPage",0);
            } else {
                model.addAttribute("nbrPage",(size/3)+1);
            }
        }
        return "back-office";
    }
}
