/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import connection.ConnectionDB;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Vector;

/**
 *
 * @author User_HP
 */
public class ObjetBdd {
    int idObjet;

    public int getIdObjet() {
        return idObjet;
    }

    public void setIdObjet(int idObjet) {
        this.idObjet = idObjet;
    }
    
/*================================= maka liste attribut class(nom,prenom,...) =============================================*/
   public String [] getAttributObject(Object obj){
        Field [] attribut = obj.getClass().getDeclaredFields();
        String [] attr = new String[attribut.length];
        for( int  i = 0 ; i < attribut.length ; i++ ){
            attr[i] = attribut[i].getName();
        }
        return attr;
    }
/*================================= maka liste type attribut class(int,String,...) =============================================*/
   public Class [] getTypeAttributObject(Object obj){
        Field [] attribut = obj.getClass().getDeclaredFields();
        Class [] clas = new Class[attribut.length];
        for( int  i = 0 ; i < attribut.length ; i++ ){
            clas[i] = attribut[i].getType();
        }
        return clas;
    }
/*================================= mamadika attribut en Majuscule 1er lettre pour get and set =================================*/
    public String [] enMajuscule(String [] attr){
        String [] attrTransfo = new String[attr.length];
        for( int  i = 0 ; i < attr.length ; i++ ){
            char [] spliter = attr[i].toCharArray();
            char unMaj = Character.toUpperCase(spliter[0]);
            spliter[0] = unMaj;
            attrTransfo[i] = String.copyValueOf(spliter);
        }
        return attrTransfo;
    }
/*================================= maka anarany class =============================================*/
    public String getNomClasse(Object obj){
        return obj.getClass().getSimpleName();
    }
/*================================= maka ireo fonctions get na set rehetra ao @ilay objet =================================*/
    public String [] getGetAndSet(String getSet,Object obj) {
        String [] attribut = this.getAttributObject(obj);
        String [] tabGetAndSet = new String[attribut.length];
        for ( int i = 0 ; i < attribut.length ; i++ ){
            tabGetAndSet[i] = getSet+(this.enMajuscule(attribut))[i];
        }
        return tabGetAndSet;
    }
/*================================= teste contenu an'ilay objet =================================*/
    public boolean testElement(Object obj){
        if( obj instanceof Integer || obj instanceof Double || obj instanceof Long ){
            if( obj instanceof Integer && ((Integer)obj).intValue() == 0 ){
                return false;
            } else if(obj instanceof Double && ((Double)obj).doubleValue() == 0.0 ){
                return false;
            } else if(obj instanceof Float && ((Float)obj).floatValue() == 0.0 ){
                return false;
            } else if(obj instanceof Long && ((Long)obj).longValue() == 0.0 ){
                return false;
            } else {
                return true;
            }
        } else {
            return true;
        }
    }
/*================================= maka getters object =================================*/
    public Method [] getValeur(Object obj) throws NoSuchMethodException{
        String [] getAndSet = this.getGetAndSet("get", obj);
        Method [] method = new Method[getAndSet.length];
        for ( int i = 0 ; i < getAndSet.length ; i++ ){
            method[i] = obj.getClass().getDeclaredMethod(getAndSet[i]);
        }
        return method;
    }
/*================================= construire requette insert =================================*/
    public String getRequetInsert(Object obj) throws Exception{
        String nomTable = this.getNomClasse(obj);
        String sql = "insert into "+nomTable;
        String colonne = "(";
        String valeur = "(";
        String [] attribut = this.getAttributObject(obj);
        Method [] method = this.getValeur(obj);
        int i = 0;
        while( i < method.length ){
            try {
                if( i == method.length - 1 ){
                    if (method[i].invoke(obj) != null ) {
                        if(this.testElement(method[i].invoke(obj))){
                            colonne += attribut[i];
                            valeur += "'"+method[i].invoke(obj).toString()+"'";
                            break;
                        } else {
                            char [] splitCol = colonne.toCharArray();
                            colonne = colonne.substring(0, splitCol.length-1);

                            char [] splitVal = valeur.toCharArray();
                            valeur = valeur.substring(0, splitVal.length-1);
                            break;
                        }
                    } else {
                        char [] splitCol = colonne.toCharArray();
                        colonne = colonne.substring(0, splitCol.length-1);
                        
                        char [] splitVal = valeur.toCharArray();
                        valeur = valeur.substring(0, splitVal.length-1);
                        break;
                    }
                } else {
                    if( method[i].invoke(obj) != null ){
                        if(this.testElement(method[i].invoke(obj))){
                            colonne += attribut[i]+",";
                            valeur += "'"+method[i].invoke(obj).toString()+"',";
                            i++;
                        } else {
                            //System.out.println(method[i].invoke(obj)+" 1");
                            i++;
                        }
                    } else {
                        //System.out.println(method[i].invoke(obj)+" 2");
                        i++;
                    }
                }
            } catch (Exception ex) {
                throw ex;
            }
        }
        colonne += ")";
        valeur += ")";
        sql += colonne+" values"+valeur;
        return sql;
    }
 /*================================= create fonction =================================*/   
    public void create(Connection c) throws Exception{
        String requetSql = this.getRequetInsert(this);
        Connection connection = c;
        if(connection == null){
            ConnectionDB con = new ConnectionDB();
            connection = con.getConnection("Postgres");
        } 
        PreparedStatement statement = null;
        ResultSet result = null;
        try{
            statement = connection.prepareStatement(requetSql);
            statement.executeUpdate();
        } catch(Exception e){
            throw e;
        } finally{
            if(result != null){
                result.close();
            }
            if(statement != null){
                statement.close();
            }
            if(connection != null){
                connection.close();
            }
        }
    }
/*================================= find fonction(fille) =================================*/   
    public Vector findFille(ResultSet result) throws Exception{
        Vector listesThis = new Vector();
        String [] attribut = this.getAttributObject(this);
        Class [] typeAttribut = this.getTypeAttributObject(this);
        String [] settersObject = this.getGetAndSet("set", this);
        try{
            while(result.next()){
                Object obj = new Object();
                obj = this.getClass().getDeclaredConstructor().newInstance();
                for ( int j = 0 ; j < settersObject.length ; j++ ){
                    Method method = obj.getClass().getMethod(settersObject[j],typeAttribut[j]);
                    if( typeAttribut[j].isAssignableFrom(String.class) ){
                        method.invoke(obj,result.getString(attribut[j]));
                    }
                    if( typeAttribut[j].isAssignableFrom(int.class) ){
                        method.invoke(obj,result.getInt(attribut[j]));
                    }
                    if( typeAttribut[j].isAssignableFrom(double.class) ){
                        method.invoke(obj,result.getDouble(attribut[j]));
                    }
                    if( typeAttribut[j].isAssignableFrom(float.class) ){
                        method.invoke(obj,result.getFloat(attribut[j]));
                    } 
                    if( typeAttribut[j].isAssignableFrom(long.class) ){
                        method.invoke(obj,result.getLong(attribut[j]));
                    }
                    if( typeAttribut[j].isAssignableFrom(Date.class) ){
                        method.invoke(obj,result.getDate(attribut[j]));
                    }
                    if( typeAttribut[j].isAssignableFrom(Timestamp.class) ){
                        method.invoke(obj,result.getTimestamp(attribut[j]));
                    }
                }
                listesThis.add(obj);
            }
        } catch(Exception e){
            throw e;
        } finally{
            if(result != null){
                result.close();
            }
        }
        return listesThis;
    }
/*================================= construire requette find(select) =================================*/  
    public String getRequetFind(Object obj)throws Exception{
        String nomTable = this.getNomClasse(obj);
        String sql = "select ";
        String [] attribut = this.getAttributObject(obj);
        for( int i = 0 ; i < attribut.length ; i++){
            if( i == attribut.length - 1 ){
                sql += attribut[i];
            } else {
                sql += attribut[i]+",";
            }
        }
        sql += " from "+nomTable+" where ";
        Method [] method = this.getValeur(obj);
        int i = 0;
        int j = 0;
        while( i < method.length ){
            try{
                if( i == method.length - 1 ){
                    if (method[i].invoke(obj) != null ) {
                        if(this.testElement(method[i].invoke(obj))){
                            sql += attribut[i]+" = '"+method[i].invoke(obj).toString()+"'";
                            j++;
                            break;
                        } else {
                            char [] splitSql = sql.toCharArray();
                            sql = sql.substring(0, splitSql.length-4);
                            break;
                        }
                    } else {
                        char [] splitSql = sql.toCharArray();
                        sql = sql.substring(0, splitSql.length-4);
                       break;
                    }
                } else {
                    if (method[i].invoke(obj) != null ) {
                        if(this.testElement(method[i].invoke(obj))){
                            sql += attribut[i]+" = '"+method[i].invoke(obj).toString()+"' and ";
                            j++;
                            i++;
                        } else {
                            i++;
                        }
                    } else {
                       i++;
                    }
                }
            } catch(Exception ex){
                throw ex;
            }
        }
        if( j == 0 ){
            char [] splitSql = sql.toCharArray();
            sql = sql.substring(0, splitSql.length-3);
        }
        return sql;
    }
 /*================================= find fonction =================================*/   
    public ArrayList<Object> find(Connection c) throws Exception{
        Connection connection = c;
        if(connection == null){
            ConnectionDB con = new ConnectionDB();
            connection = con.getConnection("Postgres");
        } 
        ArrayList<Object> listesThis = new ArrayList<>();
        String [] attribut = this.getAttributObject(this);
        Class [] typeAttribut = this.getTypeAttributObject(this);
        String [] settersObject = this.getGetAndSet("set", this);
        String requetSql = this.getRequetFind(this);
        PreparedStatement statement = null;
        ResultSet result = null;
        try{
            statement = connection.prepareStatement(requetSql);
            result = statement.executeQuery();
            int indice = 1;
            while(result.next()){
                Object obj = new Object();
                obj = this.getClass().getDeclaredConstructor().newInstance();
                for ( int j = 0 ; j < settersObject.length ; j++ ){
                    Method method = obj.getClass().getMethod(settersObject[j],typeAttribut[j]);
                    if( typeAttribut[j].isAssignableFrom(String.class) ){
                        method.invoke(obj,result.getString(attribut[j]));
                    }
                    if( typeAttribut[j].isAssignableFrom(int.class) ){
                        method.invoke(obj,result.getInt(attribut[j]));
                    }
                    if( typeAttribut[j].isAssignableFrom(double.class) ){
                        method.invoke(obj,result.getDouble(attribut[j]));
                    }
                    if( typeAttribut[j].isAssignableFrom(float.class) ){
                        method.invoke(obj,result.getFloat(attribut[j]));
                    } 
                    if( typeAttribut[j].isAssignableFrom(long.class) ){
                        method.invoke(obj,result.getLong(attribut[j]));
                    }
                    if( typeAttribut[j].isAssignableFrom(Date.class) ){
                        method.invoke(obj,result.getDate(attribut[j]));
                    }
                    if( typeAttribut[j].isAssignableFrom(Timestamp.class) ){
                        method.invoke(obj,result.getTimestamp(attribut[j]));
                    }
                }
                    listesThis.add(obj);
            }
        } catch(Exception e){
            throw e;
        } finally{
            if(result != null){
                result.close();
            }
            if(statement != null){
                statement.close();
            }
            if(connection != null){
                connection.close();
            }
        }
        return listesThis;
    }
/*================================= construire requette update =================================*/  
    public String getRequetUpdate(Object obj)throws Exception{
        String nomTable = this.getNomClasse(obj);
        String [] attribut = this.getAttributObject(obj);
        String sql = "update "+nomTable+" set ";
        Method [] method = this.getValeur(obj);
        int i = 0;
        while( i < method.length ){
            try{
                if( i == method.length - 1 ){
                    if (method[i].invoke(obj) != null ) {
                        if(this.testElement(method[i].invoke(obj))){
                            sql += attribut[i]+" = '"+method[i].invoke(obj).toString()+"'";
                            break;
                        } else {
                            char [] splitSql = sql.toCharArray();
                            sql = sql.substring(0, splitSql.length-1);
                            break;
                        }
                    } else {
                        char [] splitSql = sql.toCharArray();
                        sql = sql.substring(0, splitSql.length-1);
                       break;
                    }
                } else {
                    if (method[i].invoke(obj) != null ) {
                        if(this.testElement(method[i].invoke(obj))){
                            sql += attribut[i]+" = '"+method[i].invoke(obj).toString()+"',";
                            i++;
                        } else {
                            i++;
                        }
                    } else {
                       i++;
                    }
                }
            } catch(Exception ex){
                throw ex;
            }
        }
        sql += " where id = "+this.getIdObjet();
        return sql;
    }
/*================================= update fonction =================================*/
    public void update(Connection c) throws Exception{
        String requetSql = this.getRequetUpdate(this);
        Connection connection = c;
        if(connection == null){
            ConnectionDB con = new ConnectionDB();
            connection = con.getConnection("Postgres");
        } 
        PreparedStatement statement = null;
        ResultSet result = null;
        try{
            statement = connection.prepareStatement(requetSql);
            statement.executeUpdate();
        } catch(Exception e){
            throw e;
        } finally{
            if(result != null){
                result.close();
            }
            if(statement != null){
                statement.close();
            }
            if(connection != null){
                connection.close();
            }
        }
    }
}
