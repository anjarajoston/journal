<%-- 
    Document   : back-office
    Created on : 31 janv. 2023, 20:06:21
    Author     : User_HP
--%>
<%@page import="model.Utilisateur"%>
<%@page import="model.Contenu"%>
<%@page import="model.Lieu"%>
<%@page import="model.Type"%>
<%@page import="java.util.List"%>
<%
  Utilisateur utilisateur = (Utilisateur) request.getAttribute("utilisateur");
  List<Type> types = (List<Type>) request.getAttribute("types");
  List<Lieu> lieux = (List<Lieu>) request.getAttribute("lieux");
  List<Contenu> contenus = (List<Contenu>) request.getAttribute("contenus");
  List<Contenu> list = (List<Contenu>) request.getAttribute("list");
  int nbrPage = (Integer) request.getAttribute("nbrPage");
%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <title>Utilisateur</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat:400,400i,700,700i,600,600i">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/fonts/simple-line-icons.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.10.0/baguetteBox.min.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/css/vanilla-zoom.min.css">
</head>
<body>
<nav class="navbar navbar-light navbar-expand-lg fixed-top bg-white clean-navbar">
    <div class="container"><a class="navbar-brand logo" href="#">Utilisateur</a><button data-bs-toggle="collapse" class="navbar-toggler" data-bs-target="#navcol-1"><span class="visually-hidden">Toggle navigation</span><span class="navbar-toggler-icon"></span></button>
        <div class="collapse navbar-collapse" id="navcol-1">
            <ul class="navbar-nav ms-auto">
                <li class="nav-item"><a class="nav-link" href="<%=request.getContextPath()%>/">Home</a></li>
            </ul>
        </div>
    </div>
</nav>
<main class="page contact-us-page"><br/><br/>
<div style="float: left;">
    <main class="page contact-us-page">
        <!--section class="clean-block clean-form dark"-->
            <div class="container">
                <div class="block-heading">
                    <h1 class="text-info">Insert contenu</h1></div>
                <form action="<%= request.getContextPath() %>/addContenu" method="post">
                    <input type="hidden" value="<%=utilisateur.getId()%>" name="utilisateur">
                    <div class="mb-3">
                        <label class="form-label" for="titre">Titre :</label>
                        <input class="form-control" type="text" name="titre" id="titre">
                    </div>
                    <div class="mb-3">
                        <label class="form-label" for="visuel">Visuel :</label>
                        <input class="form-control" type="file" name="visuel" id="visuel">
                    </div>
                    <div class="mb-3">
                        <label class="form-label" for="body">Body :</label>
                        <textarea class="form-control" type="text" name="body" id="body"></textarea>
                    </div>
                    <div class="mb-3">
                        <label class="form-label" for="type">Type :</label>
                        <select class="form-control" name="type" id="type">
                            <% for (Type type: types) { %>
                            <option value="<%=type.getId()%>"><%= type.getType()%></option>
                            <% } %>
                        </select>
                    </div>
                    <div class="mb-3">
                        <label class="form-label" for="date1">Date 1 :</label>
                        <input class="form-control" type="date" name="date1" id="date1">
                    </div>
                    <div class="mb-3">
                        <label class="form-label" for="date2">Date 2 :</label>
                        <input class="form-control" type="date" name="date2" id="date2">
                    </div>
                    <div class="mb-3">
                        <label class="form-label" for="datePub">Date de publication :</label>
                        <input class="form-control" type="datetime-local" name="datePub" id="datePub">
                    </div>
                    <div class="mb-3">
                        <input type="hidden" name="lieu" value="1">
                    </div>
                    <div class="mb-3">
                        <label class="form-label" for="toerana">Lieu :</label>
                        <input class="form-control" type="text" name="toerana" id="toerana">
                    </div>
                    <div class="mb-3">
                        <button class="btn btn-primary" type="submit">Inserer</button>
                    </div>
                </form>
            </div>
        <!--/section-->
    </main>
</div>
<div class="row justify-content-center">
    <% for (int i = 0 ; i < contenus.size() ; i++ ) { %>
    <div class="col-sm-6 col-lg-4">
        <div class="card text-center clean-card">
            <img class="card-img-top w-100 d-block" src="${pageContext.request.contextPath}/resources/assets/img/<%= contenus.get(i).getVisuel()%>">
            <div class="card-body info">
                <% if(contenus.get(i).getIdType() == 1){ %>
                    <h4 class="card-title"><%= contenus.get(i).getTitre()%></h4>
                    <p>Le : <%= contenus.get(i).getDate1()%></p>
                <%}else{%>
                    <h4 class="card-title" style="color: blue;"><%= contenus.get(i).getTitre()%></h4>
                    <p>Du : <%= contenus.get(i).getDate1()%> au : <%= contenus.get(i).getDate2()%></p>
                <%}%>
                <p class="card-text">Date de publication : <%= contenus.get(i).getDatePublication()%></p>
                <p class="card-text"><%= contenus.get(i).getBody()%></p>
                <p class="card-text"><%= contenus.get(i).getLieu()%></p>
                <div class="icons"><a href="#"><i class="icon-social-facebook"></i></a><a href="#"><i class="icon-social-instagram"></i></a><a href="#"><i class="icon-social-twitter"></i></a></div>
            </div>
        </div>
    </div>
    <% } %>
</div>
    <nav>
        <ul class="pagination">
            <!-- Lien vers la page précédente (désactivé si on se trouve sur la 1ère page) -->
            <!--li class="page-item">
                <a href="" class="page-link">Précédente</a>
            </li-->
            <% for(int i = 1; i <= nbrPage; i++){ %>
            <!-- Lien vers chacune des pages (activé si on se trouve sur la page correspondante) -->
            <li class="page-item">
                <a href="<%= request.getContextPath() %>/pagination?num=<%= i %>&page=back&utilisateur=<%=utilisateur.getId()%>" class="page-link"><%= i %></a>
            </li>
            <% } %>
            <!-- Lien vers la page suivante (désactivé si on se trouve sur la dernière page) -->
            <!--li class="page-item">
                <a href="./?page=<?= $currentPage + 1 ?>" class="page-link">Suivante</a>
            </li-->
        </ul>
    </nav>
</main><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
<footer class="page-footer dark">
    <div class="container">
    </div>
    <div class="footer-copyright">
        <p>© 2023 Copyright Text</p>
    </div>
</footer>
<script src="${pageContext.request.contextPath}/resources/assets/bootstrap/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.10.0/baguetteBox.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/assets/js/vanilla-zoom.js"></script>
<script src="${pageContext.request.contextPath}/resources/assets/js/theme.js"></script>
</body>
</html>
