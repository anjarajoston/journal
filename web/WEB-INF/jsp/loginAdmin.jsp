<%-- 
    Document   : login
    Created on : 31 janv. 2023, 19:43:07
    Author     : User_HP
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
  <title>Login admin</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat:400,400i,700,700i,600,600i">
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/fonts/simple-line-icons.min.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.10.0/baguetteBox.min.css">
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/css/vanilla-zoom.min.css">
</head>
<body>
<nav class="navbar navbar-light navbar-expand-lg fixed-top bg-white clean-navbar">
  <div class="container"><a class="navbar-brand logo" href="#">Login admin</a><button data-bs-toggle="collapse" class="navbar-toggler" data-bs-target="#navcol-1"><span class="visually-hidden">Toggle navigation</span><span class="navbar-toggler-icon"></span></button>
    <div class="collapse navbar-collapse" id="navcol-1">
      <ul class="navbar-nav ms-auto">
      </ul>
    </div>
  </div>
</nav>
<main class="page contact-us-page">
  <section class="clean-block clean-form dark">
    <div class="container">
      <div class="block-heading">
        <h1 class="text-info">Login admin</h1>
        <%
          if( request.getAttribute("message") != null ){ %>
        <h4 class="text-info" style="color: red;"><%= request.getAttribute("message")%></h4>
        <% } %>
      </div>
      <form action="<%= request.getContextPath() %>/loginAdmin" method="post">
        <div class="mb-3">
          <label class="form-label" for="pseudo">Pseudo :</label>
          <input class="form-control" type="text" name="pseudo" id="pseudo">
        </div>
        <div class="mb-3">
          <label class="form-label" for="password">Password :</label>
          <input class="form-control" type="password" name="password" id="password">
        </div>
        <div class="mb-3">
          <button class="btn btn-primary" type="submit">Se connecter</button>
        </div>
      </form>
    </div>
  </section>
</main>
<footer class="page-footer dark">
  <div class="container">
  </div>
  <div class="footer-copyright">
    <p>© 2023 Copyright Text</p>
  </div>
</footer>
<script src="${pageContext.request.contextPath}/resources/assets/bootstrap/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.10.0/baguetteBox.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/assets/js/vanilla-zoom.js"></script>
<script src="${pageContext.request.contextPath}/resources/assets/js/theme.js"></script>
</body>
</html>
