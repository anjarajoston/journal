<%-- 
    Document   : index
    Created on : 31 janv. 2023, 18:09:01
    Author     : User_HP
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
  <title>Home</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat:400,400i,700,700i,600,600i">
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/fonts/simple-line-icons.min.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.10.0/baguetteBox.min.css">
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/css/vanilla-zoom.min.css">
</head>
<body>
<nav class="navbar navbar-light navbar-expand-lg fixed-top bg-white clean-navbar">
  <div class="container"><a class="navbar-brand logo" href="#">Home </a><button data-bs-toggle="collapse" class="navbar-toggler" data-bs-target="#navcol-1"><span class="visually-hidden">Toggle navigation</span><span class="navbar-toggler-icon"></span></button>
    <div class="collapse navbar-collapse" id="navcol-1">
      <ul class="navbar-nav ms-auto">
          <li class="nav-item"><a class="nav-link" href="<%=request.getContextPath()%>/front-office">Front-office</a></li>
          <li class="nav-item"><a class="nav-link" href="<%=request.getContextPath()%>/back-office">Utilisateur</a></li>
          <li class="nav-item"><a class="nav-link" href="<%=request.getContextPath()%>/admin-login">Admin</a></li>
      </ul>
    </div>
  </div>
</nav>
<main class="page contact-us-page">
  <section class="clean-block clean-form dark">
    <div class="container">
      <div class="block-heading">
        <h1 class="text-info">HOME</h1>
      </div>
    </div>
  </section>
</main>
<footer class="page-footer dark">
  <div class="container">
  </div>
  <div class="footer-copyright">
    <p>© 2023 Copyright Text</p>
  </div>
</footer>
<script src="${pageContext.request.contextPath}/resources/assets/bootstrap/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.10.0/baguetteBox.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/assets/js/vanilla-zoom.js"></script>
<script src="${pageContext.request.contextPath}/resources/assets/js/theme.js"></script>
</body>
</html>

