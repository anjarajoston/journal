<%-- 
    Document   : front-office
    Created on : 31 janv. 2023, 20:16:30
    Author     : User_HP
--%>
<%@page import="model.Admin"%>
<%@page import="model.Contenu"%>
<%@page import="java.util.List"%>
<%
    //Admin admin = (Admin)request.getAttribute("admin");
    List<Contenu> contenus = (List<Contenu>) request.getAttribute("contenus");
    List<Contenu> list = (List<Contenu>) request.getAttribute("list");
    int nbrPage = (Integer) request.getAttribute("nbrPage");
%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <title>Admin</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat:400,400i,700,700i,600,600i">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/fonts/simple-line-icons.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.10.0/baguetteBox.min.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/css/vanilla-zoom.min.css">
</head>
<body>
<nav class="navbar navbar-light navbar-expand-lg fixed-top bg-white clean-navbar">
    <div class="container"><a class="navbar-brand logo" href="#">Admin</a><button data-bs-toggle="collapse" class="navbar-toggler" data-bs-target="#navcol-1"><span class="visually-hidden">Toggle navigation</span><span class="navbar-toggler-icon"></span></button>
        <div class="collapse navbar-collapse" id="navcol-1">
            <ul class="navbar-nav ms-auto">
                <li class="nav-item"><a class="nav-link" href="<%=request.getContextPath()%>/">Home</a></li>
            </ul>
        </div>
    </div>
</nav>
<main class="page"><br/><br/>
<div style="float: left;">
    <main class="page contact-us-page">
        <!--section class="clean-block clean-form dark"-->
            <div class="container">
                <div class="block-heading">
                    <h1 class="text-info">Recherche</h1>
                </div>
                <form action="<%=request.getContextPath()%>/rechercheAdmin" method="post">
                    <div class="mb-3">
                        <label class="form-label" for="mot">Taper un mot :</label>
                        <input class="form-control" type="text" name="mot" id="mot">
                    </div>
                    <div class="mb-3">
                        <label class="form-label" for="etat">Valider :</label>
                        <select class="form-control" name="etat" id="etat">
                            <option value="2">oui</option>
                            <option value="1">non</option>
                        </select>
                    </div>
                    <div class="mb-3">
                        <button class="btn btn-primary" type="submit">Rechercher</button>
                    </div>
                </form>
            </div>
        <!--/section-->
    </main>
</div>
<div class="row justify-content-center">
    <% for (int i = 0 ; i < contenus.size() ; i++ ) { %>
    <div class="col-sm-6 col-lg-4">
        <div class="card text-center clean-card">
            <img class="card-img-top w-100 d-block" src="${pageContext.request.contextPath}/resources/assets/img/<%= contenus.get(i).getVisuel()%>">
            <div class="card-body info">
                <% if(contenus.get(i).getIdType() == 1){ %>
                    <h4 class="card-title"><%= contenus.get(i).getTitre()%></h4>
                    <p>Le : <%= contenus.get(i).getDate1()%></p>
                <%}else{%>
                    <h4 class="card-title" style="color: blue"><%= contenus.get(i).getTitre()%></h4>
                    <p>Du : <%= contenus.get(i).getDate1()%> au : <%= contenus.get(i).getDate2()%></p>
                <%}%>
                <p class="card-text">Date de publication : <%= contenus.get(i).getDatePublication()%></p>
                <p class="card-text"><%= contenus.get(i).getBody()%></p>
                <p class="card-text"><%= contenus.get(i).getLieu()%></p>
                <%if( contenus.get(i).getEtat() == 1){%>
                <form action="<%=request.getContextPath()%>/validation" method="get">
                    <div class="mb-3">Date de publication : <input type="datetime-local" name="datePub"></div>
                    <input type="hidden" value="<%=contenus.get(i).getId()%>" name="idContenu">
                    <div class="mb-3"><button class="btn btn-primary" type="submit">Valider</button></div>
                </form>
                <%}%>
                <div class="icons"><a href="#"><i class="icon-social-facebook"></i></a><a href="#"><i class="icon-social-instagram"></i></a><a href="#"><i class="icon-social-twitter"></i></a></div>
            </div>
        </div>
    </div>
    <% } %>
</div>
    <nav>
        <ul class="pagination">
            <!-- Lien vers la page précédente (désactivé si on se trouve sur la 1ère page) -->
            <!--li class="page-item">
            <a href="" class="page-link">Précédente</a>
            </li-->
            <% if (request.getAttribute("recherche")==null){%>
            <% for(int i = 1; i <= nbrPage; i++){ %>
            <!-- Lien vers chacune des pages (activé si on se trouve sur la page correspondante) -->
            <li class="page-item">
            <a href="<%= request.getContextPath() %>/pagination?num=<%= i %>&page=front" class="page-link"><%= i %></a>
            </li>
            <% } %>
            <% } %>
            <!-- Lien vers la page suivante (désactivé si on se trouve sur la dernière page) -->
            <!--li class="page-item">
            <a href="./?page=<?= $currentPage + 1 ?>" class="page-link">Suivante</a>
            </li-->
        </ul>
    </nav>

</main><br/><br/><br/><br/><br/><br/><br/><br/>
<footer class="page-footer dark">
    <div class="container">
    </div>
    <div class="footer-copyright">
        <p>© 2023 Copyright Text</p>
    </div>
</footer>
<script src="${pageContext.request.contextPath}/resources/assets/bootstrap/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.10.0/baguetteBox.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/assets/js/vanilla-zoom.js"></script>
<script src="${pageContext.request.contextPath}/resources/assets/js/theme.js"></script>
</body>
</html>
